props.globals.initNode("fdm/jsbsim/fcs/flap-cmd-bd700", 0.0);
props.globals.initNode("fdm/jsbsim/fcs/slat-cmd-bd700", 0.0);

var flaps_slats = func {

    var flap_cmd_norm = getprop("fdm/jsbsim/fcs/flap-cmd-norm");

    if (flap_cmd_norm == 0.0) {
        setprop("fdm/jsbsim/fcs/flap-cmd-bd700", 0.0);
        setprop("fdm/jsbsim/fcs/slat-cmd-bd700", 0.0);
    } else if (flap_cmd_norm == 0.2) {
        setprop("fdm/jsbsim/fcs/flap-cmd-bd700", 0.0);
        setprop("fdm/jsbsim/fcs/slat-cmd-bd700", 1.0);
    } else if (flap_cmd_norm == 0.4) {
        setprop("fdm/jsbsim/fcs/flap-cmd-bd700", 0.2);
        setprop("fdm/jsbsim/fcs/slat-cmd-bd700", 1.0);
    } else if (flap_cmd_norm == 0.8) {
        setprop("fdm/jsbsim/fcs/flap-cmd-bd700", 0.5333333333333333);
        setprop("fdm/jsbsim/fcs/slat-cmd-bd700", 1.0);
    } else if (flap_cmd_norm == 1.000) {
        setprop("fdm/jsbsim/fcs/flap-cmd-bd700", 1.0);
        setprop("fdm/jsbsim/fcs/slat-cmd-bd700", 1.0);
    }
}
setlistener("/controls/flight/flaps", flaps_slats);