props.globals.initNode("fdm/jsbsim/fcs/ground-spoilers-cmd-norm", 0.0);
props.globals.initNode("fdm/jsbsim/fcs/spoilers-armed-set", 0.0);
props.globals.initNode("gear/gear[0]/wow", 0.0);
props.globals.initNode("gear/gear[1]/wow", 0.0);

var spoilersArm = func {
    var in_air = 0.0;

    var spoilers_arm_loop = maketimer(1.5, func() {
        var spoilers_armed_set = getprop("fdm/jsbsim/fcs/spoilers-armed-set");
        var wow = (getprop("gear/gear[1]/wow") or getprop("gear/gear[2]/wow"));
        if (wow == 1.0 and spoilers_armed_set == 1.0 and in_air == 1.0) {
            setprop("fdm/jsbsim/fcs/ground-spoilers-cmd-norm", 1.0);
        }
        if (spoilers_armed_set == 0.0) {
            setprop("fdm/jsbsim/fcs/ground-spoilers-cmd-norm", 0.0);
        }
        if (wow == 1.0) {
            in_air = 0.0;
        } else {
            in_air = 1.0;
        }
    });
    spoilers_arm_loop.start();
    

}
setlistener("/sim/signals/fdm-initialized", spoilersArm);

