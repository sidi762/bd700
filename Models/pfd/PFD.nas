# ==================================================================
# BD700 PFD by IceGuye, inspired from Gijs de Rooy's Boeing 747-400
# PFD and TheTom's C130-J.
# ==================================================================

setprop("instrumentation/altimeter/in-hpa-switch", 0);
setprop("instrumentation/pfd/minima-set", 600);
setprop("instrumentation/pfd/minima-base-on", 0);

var pfdCanvas = func {

    var font_mapper = func(family,weight) {
        if (family == "'WenQuanYi Micro Hei'" and weight == "normal") {
            return "WenQuanYiMicroHei.ttf";
        } else if (family == "'Liberation Sans'" and weight == "normal") {
            return "LiberationFonts/LiberationSans-Regular.ttf";
        } else if (family == "WenQuanYi Micro Hei" and weight == "normal") {
            return "WenQuanYiMicroHei.ttf";
        } else if (family == "Liberation Sans" and weight == "normal") {
            return "LiberationFonts/LiberationSans-Regular.ttf";
        }
    };

    var my_canvas = canvas.new({
        "name": "PFD",
        "size": [1024, 1024],
        "view": [1024, 1024],
        "mipmapping": 1
    });

    var pfd = my_canvas.createGroup();

    canvas.parsesvg(
        pfd,
        "Aircraft/bd700/Models/pfd/PFD.svg",
        {'font-mapper': font_mapper}
    );

    var horizon = pfd.getElementById("horizon");
    var bankPointer = pfd.getElementById("bankPointer");
    var spdTape = pfd.getElementById("spdTape");
    var curSpdTen = pfd.getElementById("curSpdTen");
    var curSpd = pfd.getElementById("curSpd");
    var altTape = pfd.getElementById("altTape");
    var curAlt1 = pfd.getElementById("curAlt1");
    var curAlt2 = pfd.getElementById("curAlt2");
    var curAlt3 = pfd.getElementById("curAlt3");
    var tenThousand = pfd.getElementById("tenThousand");
    var baroSet = pfd.getElementById("baroSet");
    var baroUnit = pfd.getElementById("baroUnit");
    var compass = pfd.getElementById("compass");
    var vsiNeedle = pfd.getElementById("vsiNeedle");
    var vertSpd  = pfd.getElementById("vertSpd");
    var vertSpdFrame = pfd.getElementById("vertSpdFrame");
    var minimaSet = pfd.getElementById("minimaSet");
    var minimaBaseOn = pfd.getElementById("minimaBaseOn");

    horizon.set("clip", "rect(111.45, 674.12, 501.72, 330.93)");
    spdTape.set("clip", "rect(70.28, 219.92, 547.92, 135.78)");
    curSpdTen.set("clip", "rect(251.56, 208.73, 360.98, 178.51)");
    altTape.set("clip", "rect(70.78, 899.55, 548.67, 753.80)");
    curAlt3.set("clip", "rect(268.00, 899.55, 344.37, 863.37)");
    vsiNeedle.set("clip", "rect(628.73, 901.56, 978.75, 777.11)");
    var h_trans = horizon.createTransform();
    var h_rot = horizon.createTransform();

    var pfd_fast_loop = maketimer(0.04, func() {
        var pitch = getprop("orientation/pitch-deg");
        var roll =  getprop("orientation/roll-deg");
        var ias = getprop("velocities/airspeed-kt");
        var hdg_mag = getprop("orientation/heading-magnetic-deg");
        var vert_spd = getprop("velocities/vertical-speed-fps") * 60;    
        var alt = getprop("instrumentation/altimeter/indicated-altitude-ft");
    
        h_trans.setTranslation(0,pitch*7.174);
        h_rot.setRotation(-roll*D2R, horizon.getCenter());
        bankPointer.setRotation(-roll*D2R);
        spdTape.setTranslation(0, ias*5.9018);
        curSpdTen.setTranslation(0, math.mod(ias,10)*37.44);
        curSpd.setText(sprintf("%2.0f",math.floor(ias/10)));
        altTape.setTranslation(0, alt*0.4382);
        curAlt1.setText(sprintf("%2.0f",math.floor(alt/1000)));
        curAlt2.setText(sprintf("%1.0f",math.mod(math.floor(alt/100),10)));
        curAlt3.setTranslation(0, (math.mod(alt,100)/20)*21.72);
        vsiNeedle.setRotation(vert_spd*0.02018*D2R);
        vertSpd.setText(sprintf("%5.0f",vert_spd));
        compass.setRotation(-hdg_mag*D2R);    
    });
    pfd_fast_loop.start();

    var pfd_slow_loop = maketimer(0.25, func() {
        var baro_inhg = getprop("instrumentation/altimeter/setting-inhg");
        var baro_hpa = getprop("instrumentation/altimeter/setting-hpa");
        var in_hpa_switch = getprop("instrumentation/altimeter/in-hpa-switch");
        var vert_spd = getprop("velocities/vertical-speed-fps") * 60;
        var alt = getprop("instrumentation/altimeter/indicated-altitude-ft");
        var minima_base_on = getprop("instrumentation/pfd/minima-base-on");
        var minima_set = getprop("instrumentation/pfd/minima-set");

        if (alt < 10000) {
            tenThousand.show();
        } else {
            tenThousand.hide();
        }
    
        if (in_hpa_switch == 0) {
            baroSet.setText(sprintf("%2.2f", baro_inhg));
	    baroUnit.setText(sprintf("%s", "IN"));
        } else {
            baroSet.setText(sprintf("%4.0f", baro_hpa));
	    baroUnit.setText(sprintf("%s", "HPa"));
        }

        if (math.abs(vert_spd) < 500) {
            vertSpd.hide();
	    vertSpdFrame.hide();
        } else {
            vertSpd.show();
	    vertSpdFrame.show();
        }

        minimaSet.setText(sprintf("%4.0f", minima_set));

        if (minima_base_on == 0) {
            minimaBaseOn.setText(sprintf("%s", "RAD"));
        } else {
            minimaBaseOn.setText(sprintf("%s", "BARO"));
        }    
    });
    pfd_slow_loop.start();
    
    my_canvas.addPlacement({"node": "PFD-Screen"});
}

setlistener("sim/signals/fdm-initialized", pfdCanvas);
